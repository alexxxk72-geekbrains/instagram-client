package ru.geekbrains.instagram;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.geekbrains.instagram.data.PrefsData;

public class SelectedAppThemeFragment extends Fragment {

    @BindView(R.id.select_theme) RadioGroup radioSelectTheme;
    @BindView(R.id.radio_original) RadioButton radioOriginal;
    @BindView(R.id.radio_custom) RadioButton radioCustom;
    PrefsData prefsData;

    public SelectedAppThemeFragment() {
    }

    public static SelectedAppThemeFragment newInstance() {
        SelectedAppThemeFragment fragment = new SelectedAppThemeFragment();
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selected_app_theme, container, false);
        ButterKnife.bind(this, view);
        prefsData = new PrefsData(getActivity());
        initUi();
        initListener();
        return view;
    }

    private void initUi() {
        if(prefsData.getSharedPreferences(PrefsData.THEME_NAME)
                .equals(String.valueOf(R.style.AppTheme1_NoActionBar)))
                radioOriginal.setChecked(true);
        if(prefsData.getSharedPreferences(PrefsData.THEME_NAME)
                .equals(String.valueOf(R.style.AppTheme2_NoActionBar)))
            radioCustom.setChecked(true);
    }

    public void initListener(){
        radioSelectTheme.setOnCheckedChangeListener((group, checkedId) ->
                onChangeRadioSelectTheme(group, checkedId));
    }

    private void onChangeRadioSelectTheme(RadioGroup group, int checkedId){
        switch (checkedId){
            case R.id.radio_original:
                prefsData.saveSharedPreferences(PrefsData.THEME_NAME,
                        String.valueOf(R.style.AppTheme1_NoActionBar));
                break;
            case R.id.radio_custom:
                prefsData.saveSharedPreferences(PrefsData.THEME_NAME,
                        String.valueOf(R.style.AppTheme2_NoActionBar));
                break;
        }
        ((BaseActivity)getActivity()).restart();
    }
}
