package ru.geekbrains.instagram;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String EMPTY_STRING = "";

    @BindView(R.id.layout_login)
    TextInputLayout layoutLogin;
    @BindView(R.id.txt_login)
    EditText txtLogin;
    @BindColor(R.color.accent1)
    int colorError;
    private Pattern pLogin = Pattern.compile("^[a-zA-Z0-9]*$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //layoutLogin.setErrorTextColor(colorError);
        txtLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Matcher m = pLogin.matcher(s);
                if (!m.matches()) {
                    layoutLogin.setError("только латинские буквы и цифры");
                } else layoutLogin.setError(EMPTY_STRING);
            }
        });
    }
}
