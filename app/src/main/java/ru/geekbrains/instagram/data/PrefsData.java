package ru.geekbrains.instagram.data;

import android.content.Context;
import android.content.SharedPreferences;
import ru.geekbrains.instagram.App;
import static android.content.Context.MODE_PRIVATE;


public class PrefsData implements PrefsHelper {

    private SharedPreferences sharedPreferences;
    private static PrefsData instanse = new PrefsData();

    private PrefsData() {
        // sharedPreferences = App.getInstanse().getSharedPreferences(PREFS_NAME, MODE_PRIVATE); не работает
    }

    public PrefsData(Context context){
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
    }

    public static PrefsData getInstanse() {
        return instanse;
    }

    @Override
    public String getSharedPreferences(String keyPref) {
        return sharedPreferences.getString(keyPref, "");
    }

    @Override
    public void saveSharedPreferences(String keyPref, String value) {
        sharedPreferences.edit().putString(keyPref, value).apply();
    }

    @Override
    public void deleteSharedPreferences(String keyPref) {
        sharedPreferences.edit().remove(keyPref).apply();
    }

}
