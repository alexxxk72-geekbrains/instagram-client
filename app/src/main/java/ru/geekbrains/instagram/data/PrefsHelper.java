package ru.geekbrains.instagram.data;

/**
 * Created by shkryaba on 28/07/2018.
 */

public interface PrefsHelper {
    String PREFS_NAME = "name_shared_preferences";
    String THEME_NAME = "app_theme";

    String getSharedPreferences(String keyPref);

    void saveSharedPreferences(String keyPref, String value);

    void deleteSharedPreferences(String keyPref);
}
