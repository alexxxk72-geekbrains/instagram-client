package ru.geekbrains.instagram;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import ru.geekbrains.instagram.data.PrefsData;


public class BaseActivity extends AppCompatActivity {

    PrefsData prefsData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefsData = new PrefsData(this);
        if (prefsData.getSharedPreferences(PrefsData.THEME_NAME).equals("")) {
            prefsData.saveSharedPreferences(PrefsData.THEME_NAME, String.valueOf(R.style.AppTheme1_NoActionBar));
        }else{
            setTheme(Integer.valueOf(prefsData.getSharedPreferences(PrefsData.THEME_NAME)));
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        initUi();
        addFragment(BaseFragment.newInstance());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav_menu, menu);
        return true;
    }

    private void initUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void addFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                addFragment(BaseFragment.newInstance());
                break;
            case R.id.nav_style:
                addFragment(SelectedAppThemeFragment.newInstance());
                break;
        }
        return true;
    }

    public void restart() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            recreate();
        } else {
            finish();
            Intent intent = new Intent(this, BaseActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
