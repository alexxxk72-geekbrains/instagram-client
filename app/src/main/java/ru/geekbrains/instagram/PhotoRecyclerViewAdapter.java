package ru.geekbrains.instagram;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;

import ru.geekbrains.instagram.model.Photo;
import ru.geekbrains.instagram.model.PhotoAlbum;


public class PhotoRecyclerViewAdapter extends RecyclerView.Adapter<PhotoRecyclerViewAdapter.ViewHolder> {

    private final static String STRING_EMPTY = "";
    private final List<Photo> listPhoto;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
    private final BaseFragment.OnListFragmentInteractionListener mListener;

    public PhotoRecyclerViewAdapter(List<Photo> list, BaseFragment.OnListFragmentInteractionListener listener) {
        listPhoto = list;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_photo_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.item = listPhoto.get(position);
        Picasso.with(holder.mPhoto.getContext())
                .load(new File(holder.item.path)).into(holder.mPhoto);
        if (holder.item.isFavorite) {
            Picasso.with(holder.mFavorite.getContext())
                    .load(R.mipmap.favorite).into(holder.mFavorite);
        } else {
            Picasso.with(holder.mFavorite.getContext())
                    .load(R.mipmap.no_favorite).into(holder.mFavorite);
        }
        if (holder.item.comment != null) {
            holder.mComment.setText(holder.item.comment);
        } else {
            holder.mComment.setText(STRING_EMPTY);
        }
        if (holder.item.dateShooting != null) {
            holder.mDate.setText(dateFormat.format(holder.item.dateShooting));
        } else {
            holder.mDate.setText(STRING_EMPTY);
        }

        holder.itemView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onListFragmentInteraction(holder.item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listPhoto.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mDate;
        public final ImageView mPhoto;
        public final ImageView mFavorite;
        public final TextView mComment;
        public Photo item;


        public ViewHolder(View view) {
            super(view);
            mPhoto = view.findViewById(R.id.photo_view);
            mFavorite = view.findViewById(R.id.is_favorites);
            mDate = view.findViewById(R.id.txt_date);
            mComment = view.findViewById(R.id.txt_comment);
        }
    }
}
