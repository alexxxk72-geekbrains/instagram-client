package ru.geekbrains.instagram;

import android.app.Application;

public class App extends Application {

    private static App instanse;

    @Override
    public void onCreate() {
        super.onCreate();
        instanse = this;
    }

    public static App getInstanse() {
        return instanse;
    }
}
