package ru.geekbrains.instagram.model;

import java.util.Date;

public class Photo {
    public final Date dateShooting;
    public String path;
    public String comment;
    public boolean isFavorite;

    public Photo() {
        this.dateShooting = new Date();
        this.isFavorite = false;
    }
}
