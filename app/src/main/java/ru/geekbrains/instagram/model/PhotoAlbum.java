package ru.geekbrains.instagram.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PhotoAlbum {

    private List<Photo> album;

    public PhotoAlbum(String path) {
        File directory = new File(path);
        if (directory.exists() && directory.isDirectory()) {
            album = new ArrayList<>();
            File[] files = directory.listFiles();
            for (File file : files) {
                Photo photo = new Photo();
                photo.path = file.toString();
                addItem(photo);
            }
        }
    }

    public List<Photo> getAlbum() {
        return album;
    }

    public void addItem(Photo item) {
        album.add(item);
    }
}
