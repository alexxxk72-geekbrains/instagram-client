package ru.geekbrains.instagram;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.geekbrains.instagram.model.Photo;
import ru.geekbrains.instagram.model.PhotoAlbum;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class BaseFragment extends Fragment {


    private final static String TAG = "BaseFragment";
    private final static int CAMERA_REQUEST = 1;
    @BindView(R.id.btn_add_photo)
    FloatingActionButton btnAddPhoto;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private PhotoRecyclerViewAdapter photoAdapter;
    private OnListFragmentInteractionListener mListener;
    private PhotoAlbum photoAlbum;
    private Photo mPhoto;

    public BaseFragment() {
    }

    public static BaseFragment newInstance() {
        BaseFragment fragment = new BaseFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base, container, false);
        ButterKnife.bind(this, view);
        initUi();
        initListener();
        return view;
    }

    public void initUi() {
        photoAlbum = new PhotoAlbum(getPhotoDirectory().toString());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        photoAdapter = new PhotoRecyclerViewAdapter(photoAlbum.getAlbum(), mListener);
        recyclerView.setAdapter(photoAdapter);
    }

    public void initListener() {
        btnAddPhoto.setOnClickListener((v) -> fabAddPhotoOnclick());
    }

    private void fabAddPhotoOnclick() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, generateFileUri());
            startActivityForResult(intent, CAMERA_REQUEST);
        } catch (Exception e) {
            Log.e(TAG, "Ошибка добавления фото: " + e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent intent) {
        if (requestCode == CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                photoAlbum.addItem(mPhoto);
                photoAdapter.notifyItemInserted(photoAdapter.getItemCount() - 1);
                mPhoto = null;
                getSnackbar();
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "Canceled");
            }
        }
    }

    public void getSnackbar() {
        Snackbar mSnackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                "Фото добавлено", Snackbar.LENGTH_LONG);
        View mSnackbarView = mSnackbar.getView();
        mSnackbarView.setElevation(6);
        mSnackbarView.setBackgroundColor(getActivity().getResources().getColor(R.color.primary_transp));
        TextView tv = mSnackbarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextSize(25);
        mSnackbar.show();
    }

    private Uri generateFileUri() {
        File file = new File(getPhotoDirectory().getPath() + "/" + "photo_"
                + System.currentTimeMillis() + ".jpg");
        mPhoto = new Photo();
        mPhoto.path = file.toString();
        mPhoto.comment = "Comment";
        return FileProvider.getUriForFile(getContext(), getContext().getApplicationContext()
                .getPackageName() + ".provider", file);
    }

    private File getPhotoDirectory() {
        File directory =
                getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!directory.exists())
            directory.mkdirs();
        return directory;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Photo item);
    }
}
